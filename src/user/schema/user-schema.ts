import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type UserDocument = User & Document

@Schema({collection:'User'})
export class User {
  
    @Prop()
    firstName:string;
  
    @Prop()
    lastName:string;
  
    @Prop()
    email:string;
  
    @Prop()
    phoneNumber:number;

    @Prop()
    age:number;
}
export const UserSchema = SchemaFactory.createForClass(User);