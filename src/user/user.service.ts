import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schema/user-schema';
import { Model } from 'mongoose';
import { UpdateUserdto } from './dto/updateuserDto';


@Injectable()
export class UserService {
    constructor(@InjectModel(User.name) private userModel : Model<UserDocument>){}

    async getAll(): Promise<User[]> {
        const users = await this.userModel.find().sort({ _id: -1 }).exec();
        return users;
      }

    async create(user:User){
        const newUserr = new this.userModel(user);
       return  await newUserr.save();

    }
    async getbyId(id:string){
        return await this.userModel.findById(id).exec();
    }
    async update(id:string,user:UpdateUserdto){
        return await this.userModel.findByIdAndUpdate(id,user,{new:true})
    }

    async delete(id :string){
        return await this.userModel.findByIdAndRemove(id)
    }
    async checkEmailExists(email: string): Promise<boolean> {
        const existingUser = await this.userModel.findOne({ email });
        return !!existingUser;
    }
}



