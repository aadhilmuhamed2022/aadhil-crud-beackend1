import { Body, ConflictException, Controller, Delete, Get, NotFoundException, Param, Post, Put } from '@nestjs/common';
import { UserService } from './user.service';

import { UserDto } from './dto/createuserDto';
import { UpdateUserdto } from './dto/updateuserDto';

@Controller('user')
export class UserController {
    constructor (private readonly userService:UserService){}

  @Get()
  async getAll(){
        return await this.userService.getAll();
  }

  @Post()
async create(@Body() userDto: UserDto) {
  const emailExists = await this.userService.checkEmailExists(userDto.email);
  if (emailExists) {
    throw new ConflictException('Email already exists');
  }
  return await this.userService.create(userDto);
}
  @Get(":id")
  async getbyId(@Param('id')id:string){
    return await this.userService.getbyId(id);

  }
  @Put(":id")
async update(@Param('id') id: string, @Body() user: UpdateUserdto) {
  try {
    const updatedUser = await this.userService.update(id, user);
    return { message: 'User updated successfully', data: updatedUser };
  } catch (err) {
    return { message: 'Failed to update user', error: err.message };
  }
}


  @Delete(":id")
  async delete(@Param('id') id: string) {
    const deletedUser = await this.userService.delete(id);

    if (!deletedUser) {
      throw new NotFoundException(`User with id ${id} not found`);
    }

    return { message: `User with id ${id} deleted` };
  }
}