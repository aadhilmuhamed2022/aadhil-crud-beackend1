import { PartialType } from '@nestjs/mapped-types';
import { UserDto } from './createuserDto';
import { IsEmail, IsNotEmpty, IsNumber, IsString, MaxLength } from 'class-validator';
export class UpdateUserdto 
// extends PartialType(UserDto)
 {

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    firstName:string;

    @IsString()
    @MaxLength(30)
    @IsNotEmpty()
    lastName:string;

   
    // email:string;
    // phoneNumber:number;
    
    @IsNumber()
    @IsNotEmpty()
    age:number;












}