import { IsEmail, IsNotEmpty, IsNumber, IsPhoneNumber, IsString, MaxLength } from "class-validator";

export class UserDto{
    
    @IsString()
    @MaxLength(50)
    @IsNotEmpty()
    firstName:string;

    @IsString()
    @MaxLength(50)
    @IsNotEmpty()
    lastName:string;

    @IsEmail()
    @IsNotEmpty()
    email:string;

    @IsNumber()
    @IsNotEmpty()
    phoneNumber:number;
    
    @IsNumber()
    @IsNotEmpty()
    age:number;

}