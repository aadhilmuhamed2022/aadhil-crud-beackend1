


import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { UserController } from './user/user.controller';
import { UserService } from './user/user.service';


@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://Aadhil:rK5EBCdth5cm2Pi@crud.2hneitx.mongodb.net/crud?retryWrites=true&w=majority'),
    UserModule
  
  ],

  controllers: [AppController,UserController],
  providers: [AppService],
})
export class AppModule {}
