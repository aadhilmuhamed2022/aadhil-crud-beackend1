export declare class UserDto {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: number;
    age: number;
}
